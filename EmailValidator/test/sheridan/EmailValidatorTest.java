package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testEmailRegular() {
		String email="wilson@gmail.com";
		assertTrue("This is a valid email address.", EmailValidator.isValidEmail(email));
	}

	@Test
	public void testEmailFormatException() {
		String email="";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailFormatBoundaryIn() {
		String email="n2n@gmail.com";
		assertTrue("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailFormatBoundaryOut() {
		String email="@gmail.com";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailSymbolException() {
		String email="namegmail.com";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailSymbolBoundaryIn() {
		String email="abc@gmail.com";
		assertTrue("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailSymbolBoundaryOut() {
		String email="abE1@gmai@l.com";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailAccountException() {
		String email="999@gmail.com";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailAccountBoundaryIn() {
		String email="abc@gmail.com";
		assertTrue("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailAccountBoundaryOut() {
		String email="1abcE1@gmail.com";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailDomainException() {
		String email="abc@.com";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailDomainBoundaryIn() {
		String email="abc@gmail123.com";
		assertTrue("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailDomainBoundaryOut() {
		String email="test@ca.com";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	
	@Test
	public void testEmailExtensionException() {
		String email="abc@gmail.";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailExtensionBoundaryIn() {
		String email="abc@gmail.ca";
		assertTrue("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testEmailExtensionBoundaryOut() {
		String email="test@gmail.c";
		assertFalse("This is a valid email address.", EmailValidator.isValidEmail(email));
	}
}
