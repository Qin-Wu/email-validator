package sheridan;

public class EmailValidator {

	public static boolean isValidEmail(String email) {

		boolean validEmail = true;

		if (!email.matches("[a-zA-Z][\\w\\.\\-]+@([a-z0-9][a-z0-9][a-z0-9]+\\.)+[a-zA-Z][a-zA-Z]+")) {
			validEmail = false;
		}

		return validEmail;

	}
}
